package com.deeper.deeper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DrawThread extends Thread {
    //--------------------VARIABLES--------------------
    private static final int SPEED = 10;
    private static final int LOAD_SIZE = 400;
    private Paint paint;
    private List<String[]> dotList;
    private SurfaceHolder holder;
    private boolean running = true;
    private int pixelWidth, width, height;
    private CSVFile csvFile;
    private InputStream inputStream;

    //-------------------MAIN-METHODS-----------------
    public DrawThread(Context context, SurfaceHolder holder) {
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        this.holder = holder;

        try {
            inputStream = context.getAssets().open("deeperRawData.csv");
            csvFile = new CSVFile(inputStream);
            csvFile.setFrom(0);
            csvFile.setTo(LOAD_SIZE * 2);
            dotList = csvFile.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        pixelWidth = width / LOAD_SIZE;
    }

    @Override
    public void run() {
        while (running) {
            Canvas canvas = holder.lockCanvas(null);

            if (canvas == null) {
                running = false;
            } else {
                onDraw(canvas);
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    //TODO introduce time variable to have stable frame rate
    public void onDraw(@NonNull Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        int x = 0;
        int numberOfPixelRows = dotList.size();
        int pixelsPerWidth = (width < numberOfPixelRows) ? width / pixelWidth % numberOfPixelRows : numberOfPixelRows;

        for (int i = 0; i < pixelsPerWidth; i++) {
            String[] onePixelLine = dotList.get(i);
            float pixelIncrement = height / (float) onePixelLine.length;
            int y = 0;

            for (String pixelColor : onePixelLine) {
                int colorCode = Integer.parseInt(pixelColor);
                paint.setColor(Color.rgb(0, colorCode, colorCode));
                canvas.drawRect(x, y, x + pixelWidth, y += pixelIncrement, paint);
            }
            x += pixelWidth;
        }

        if (dotList.size() > SPEED && x > width) {
            removeOffset();
        } else {
            running = false;
        }
    }

    protected void removeOffset() {
        for (int i = 0; i < SPEED; i++) {
            dotList.remove(i);
        }

        if (dotList.size() < LOAD_SIZE * 1.5f) {
            readData();
        }
    }

    protected void readData() {
        csvFile.setFrom(csvFile.getTo());
        csvFile.setTo(csvFile.getTo() + LOAD_SIZE * 2);

        try {
            dotList.addAll(csvFile.read());
        } catch (IOException e) {
            e.printStackTrace();
            csvFile.setFrom(csvFile.getFrom() - LOAD_SIZE * 2);
            csvFile.setTo(csvFile.getTo() - LOAD_SIZE * 2);
        }
    }

    public void closeInputStream() {
        if (inputStream == null) {
            return;
        }

        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
