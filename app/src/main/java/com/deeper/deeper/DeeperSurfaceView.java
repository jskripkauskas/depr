package com.deeper.deeper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class DeeperSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    //--------------------VARIABLES--------------------
    private SurfaceHolder holder;
    private DrawThread drawThread;

    //-------------------MAIN-METHODS-----------------
    public DeeperSurfaceView(Context context) {
        super(context);
        holder = getHolder();
        holder.addCallback(this);
    }

    public DeeperSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        holder = getHolder();
        holder.addCallback(this);
    }

    public DeeperSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        holder = getHolder();
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        //give time to instantiate other stuff
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                drawThread = new DrawThread(getContext(), holder);
                drawThread.start();
            }
        }, 1000l);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        drawThread.closeInputStream();
        drawThread.interrupt();
    }

}
