package com.deeper.deeper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVFile {
    //--------------------VARIABLES--------------------
    private InputStream inputStream;
    private int from = 0, to = 400;

    //-------------------MAIN-METHODS-----------------
    public CSVFile(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public List<String[]> read() throws IOException {
        List<String[]> resultList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String csvLine;
        int count = 0;

        while ((csvLine = reader.readLine()) != null && count < to) {
            String[] row = csvLine.split("\t");

            if (row.length > 1) {
                count++;
                if (count >= from) {
                    resultList.add(row);
                }
            }
        }
        return resultList;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}